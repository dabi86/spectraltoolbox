#!/bin/bash

rm -rf dist
pip install --upgrade twine

pip wheel --no-deps -w dist .

twine upload dist/**.whl


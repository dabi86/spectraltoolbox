#!/bin/bash
set -e -u -x

function repair_wheel {
    wheel="$1"
    if ! auditwheel show "$wheel"; then
        echo "Skipping non-platform wheel $wheel"
    else
        auditwheel repair "$wheel" --plat "$PLAT" -w dist/
    fi
}

# Install a system package required by our library

# Compile wheels
for PYBIN in /opt/python/*/bin; do
    if [ $(${PYBIN}/python -c 'import sys; from packaging import version; print(int(version.parse(sys.version.split(" ")[0]) < version.parse("3.8")))') -eq 1 ] ; then
        continue
    fi
    # "${PYBIN}/pip" install -r /io/dev-requirements.txt
    "${PYBIN}/pip" install --upgrade pip
    "${PYBIN}/pip" wheel . --no-deps -w wheelhouse/
done

# Bundle external shared libraries into the wheels
for whl in wheelhouse/*.whl; do
    repair_wheel "$whl"
done

# chmod a+w wheelhouse/*
# chmod a+w dist/*

## Install packages and test
#for PYBIN in /opt/python/*/bin/; do
#    "${PYBIN}/pip" install python-manylinux-demo --no-index -f /io/wheelhouse
#    (cd "$HOME"; "${PYBIN}/nosetests" pymanylinuxdemo)
#done